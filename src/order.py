from flask import Flask
from flask import request
from flask import render_template
from flask import jsonify
import time
import json
from flask import Flask
from flask_limiter import Limiter


order = Flask(__name__)
order.debug = True
limiter = Limiter(app, global_limits=["2 per minute", "1 per second"])

####################################################################################
#   Error handler
####################################################################################

#@order.errorhandler(Exception)
#def all_exception_handler(error):
#    err_json = {}
#    err_json['status'] = 'Error'
#    #return 'Error', 500
#    return jsonify(err_json)

####################################################################################


@order.route('/')
@limiter.limit("1 per second")
def order_form():
    return render_template("index.html")

@order.route('/orders', methods=['POST'])
@limiter.limit("1 per second")
def order_form_post():
    try:
        r = request.get_json(force=True) 
        resp = {}
        resp['name'] = r['name']
        resp['mobile'] = r['tel']
        resp['status'] = 'Success'
        resp['time'] = time.asctime( time.localtime(time.time()) ) 
        return jsonify(resp)
    except:
        #traceback.print_exc(file=sys.stdout)
	resp = {}
        resp['name'] = ''
        resp['mobile'] = ''
        resp['status'] = 'Failed'
        resp['time'] = time.asctime( time.localtime(time.time()) )
        return jsonify(resp)


if __name__ == '__main__':
    order.run(
	host = '0.0.0.0',
	port = 5000
	)
